import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { useState } from 'react';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

const EditableTypography = (props:any) => {
  const validation = props.validation ?? (() => true);
  const [isEditMode, setEditMode] = useState(false);
  const [isValid, setIsValid] = useState(true);
  const [value, setValue] = useState(props.value);

  const handleEdit = (e:any) => {
    // ESC pressed
    if (e.keyCode === 27) {
      return setEditMode(false);
    }

    // ENTER pressed
    if (e.keyCode === 13) {
      return setEditMode(false);
      // TODO: make API call
    }
    
    console.log({ currentValue: e.target.value });
  };

  return (
    <Typography {...props} onClick={() => setEditMode(true) }>
      {isEditMode && <input type="text" value={value} onKeyDown={handleEdit} onChange={(e) => setValue(e.target.value)} />} 
      {!isEditMode && props.value} 
    </Typography>
  );
};

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <EditableTypography variant="subtitle1" lineHeight="1rem" validation={(v:string) => v === "renan" } value={name} />
          <EditableTypography variant="caption" color="text.secondary" value={email} />
        </Box>
      </Box>
    </Card>
  );
};
